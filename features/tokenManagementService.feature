# @author Ion s204710
Feature: Tokens requested features

  Scenario: Successful token generation
    Given a new TokenService, a "TokensGenerationRequested" event, a customerId "cid2" and an amount 3
    When the generateTokens method is run
    Then the "TokensGenerated" event is sent with the correct amount

  Scenario: Error event when requesting more than 5 tokens
    Given a new TokenService, a "TokensGenerationRequested" event, a customerId "cid22" and an amount 7
    When the generateTokens method is run
    Then the "TokensGenerationFailed" event is sent instead

  Scenario: Error event when requesting less than 1 token
    Given a new TokenService, a "TokensGenerationRequested" event, a customerId "cid222" and an amount 0
    When the generateTokens method is run
    Then the "TokensGenerationFailed" event is sent instead

  Scenario: Error event when requesting tokens when costumer already has more than 1
    Given a customerId "cid98" whose corresponding customer has 2 tokens
    When the generateTokens method is run
    Then the "TokensGenerationFailed" event is sent instead

  Scenario: Token Generation Race condition
    Given a new TokenService, two "TokensGenerationRequested" events, two customerIds "cid5678", "cid1234", and two amounts 3 and 4
    When the two events are sent at the same time
    Then the the two "TokensGenerated" events are sent with the correct amount

  Scenario: Token validated
    Given a token and a customerId "cid24323"
    And customer has 2 tokens
    When the event "PaymentRequested" is received
    Then the "TokenValidated" event is sent

  Scenario: Error when token not validated
    Given a token and a customerId "cid24323"
    And the token has been spent
    When the event "PaymentRequested" is received
    Then the "TokenNotValidated" event is sent instead

  Scenario: Error when trying to get token in repository when it doesn't exist
    Given a token and a customerId "cid24323"
    And the token has been spent
    When the getCustomerId method is run
    Then the method should return null
