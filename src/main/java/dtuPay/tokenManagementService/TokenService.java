package dtuPay.tokenManagementService;

import dtuPay.tokenManagementService.model.*;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;

// @author Can s232851
public class TokenService {

	private MessageQueue queue;

	TokenRepository tokenRepository = TokenRepository.getInstance();


	public String getCustomerId(Token token){
		return tokenRepository.getCustomerId(token);
	}
	public TokenService(MessageQueue q) {
		queue = q;
		queue.addHandler("PaymentRequested", this::validateToken);
		queue.addHandler("TokensGenerationRequested", this::generateTokens);
	}

	public void generateTokens(Event event) {

		var tokenRequest = event.getArgument(0, TokenRequest.class);
		int tokenAmount = tokenRequest.getAmount();
		CorrelationId correlationId = event.getArgument(1, CorrelationId.class);
		String customerId = tokenRequest.getCustomerId();

		try{
			ArrayList<Token> generatedTokens = tokenRepository.generateTokens(tokenAmount, customerId);
			queue.publish(new Event("TokensGenerated", new Object[]{generatedTokens, correlationId}));
		} catch (Exception tokenException){
			queue.publish(new Event("TokensGenerationFailed", new Object[]{tokenException.getMessage(), correlationId}));
		}
	}

	public void validateToken(Event e) {
		PaymentRequest paymentRequest = e.getArgument(0, PaymentRequest.class);
		CorrelationId correlationId = e.getArgument(1, CorrelationId.class);
		Token token = paymentRequest.getToken();
		Event event;
		if (tokenRepository.tokenValid(token)) {
			ValidatedToken validatedToken = new ValidatedToken();
			validatedToken.setCustomerID(tokenRepository.getCustomerId(token));
			event = new Event("TokenValidated", new Object[] { validatedToken, correlationId });
			tokenRepository.removeToken(token);
		} else {
			event = new Event("TokenNotValidated", new Object[] { "Token not validated", correlationId });
		}
		queue.publish(event);
	}
}
