package dtuPay.tokenManagementService;

import dtuPay.tokenManagementService.model.Token;

import java.util.ArrayList;
import java.util.HashMap;

// @author Alexander s204092
public class TokenRepository {
    private HashMap<Token, String> tokenList = new HashMap<>();
    private static  TokenRepository instance = null;

    public static TokenRepository getInstance() {
        if (instance == null) {
            instance = new TokenRepository();
        }
        return instance;
    }

    public TokenRepository() {
    }

    public void addToken(Token token, String id) {
        tokenList.put(token, id);

    }

    public String getCustomerId(Token token) {
        for (Token t : tokenList.keySet()) {
            if (t.getTokenID().equals(token.getTokenID())) {
                return tokenList.get(t);
            }
        }
        return null;
    }

    public boolean tokenValid(Token token) {
        return  tokenList.containsKey(token);
    }

    public ArrayList<Token> generateTokens(int amount, String customerId) throws Exception {
        if (amountOfTokens(customerId)>1){
            throw new Exception("Customer already has available tokens");
        }

        if (amount>5 || amount<1) {
            throw new Exception("Amount of tokens requested must be between 1 and 5");
        }

        ArrayList<Token> tokens = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            Token token = new Token();
            token.setRandomID();
            tokens.add(token);
        }
        for (Token token : tokens) {
            addToken(token, customerId);
        }
        return tokens;
    }

    public int amountOfTokens(String costumerId) {
        return (int) tokenList.values().stream().filter((String c) -> c.equals(costumerId)).count();
    }

    public void removeToken(Token token) {tokenList.remove(token);}

}
