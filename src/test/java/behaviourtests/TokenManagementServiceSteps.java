package behaviourtests;


import dtuPay.tokenManagementService.TokenRepository;
import dtuPay.tokenManagementService.TokenService;
import dtuPay.tokenManagementService.model.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TokenManagementServiceSteps {

    Token token = new Token();
    String customerId = "123";
    String customerId1, customerId2;
    int amount1, amount2;
    TokenRepository tr = TokenRepository.getInstance();
    Exception error = new Exception();
    String errormsg;
    Event event;
    Event event1, event2;
    TokenRequest tokenRequest;
    TokenRequest tokenRequest1, tokenRequest2;
    int amount;
    CorrelationId correlationId;
    CorrelationId correlationId1, correlationId2, correlationId3;
    private Map<CorrelationId, CompletableFuture<Event>> publishedEvents = new HashMap<>();
    private MessageQueue queue = new TestMessageQueue();
    TokenService tokenService = new TokenService(queue);

    String result;

    @Given("a token and a customerId {string}")
    public void aTokenAndAnCustomerId(String arg0) {
        token.setRandomID();
        customerId = arg0;
    }

    @And("customer has {int} tokens")
    public void customerHasTokens(int arg0) throws Exception {
        ArrayList<Token> tokens = tr.generateTokens(arg0, customerId);
        token = tokens.get(0);
    }

    @When("the event {string} is received")
    public void the_event_is_received(String string) {
        correlationId = CorrelationId.randomId();
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setToken(token);
        publishedEvents.put(correlationId, new CompletableFuture<Event>());
        tokenService.validateToken(new Event(string, new Object[]{paymentRequest, correlationId}));
    }

    @Then("the {string} event is sent")
    public void the_event_is_sent(String string) {
        ValidatedToken validatedToken = new ValidatedToken();
        validatedToken.setCustomerID(customerId);
        Event event = publishedEvents.get(correlationId).join();
        assertEquals(string, event.getType());
        ValidatedToken result = event.getArgument(0, ValidatedToken.class);
        assertEquals(result, validatedToken);
    }

    @Given("a token with tokenID null and a customerId {string}")
    public void aTokenWithTokenIDNullAndACustomerId(String arg0) {
        token.setTokenID(null);
        customerId = arg0;
    }

    @When("the addToken method is run")
    public void theAddTokenMethodIsRun() {
        try {
            tr.addToken(token, customerId);
        } catch (Exception e) {
            error = e;
            errormsg = e.getMessage();
        }
    }

    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String arg0) {
        Assertions.assertNull(token.getTokenID());
        Assertions.assertEquals(arg0, errormsg);
    }

    @Given("a new TokenService, a {string} event, a customerId {string} and an amount {int}")
    public void aNewTokenServiceAndAEvent(String arg0, String arg1, int arg2) {
        tokenService = new TokenService(queue);
        customerId = arg1;
        amount = arg2;
        tokenRequest = new TokenRequest();
        tokenRequest.setAmount(arg2);
        tokenRequest.setCustomerId(arg1);
        correlationId = CorrelationId.randomId();
        event = new Event(arg0, new Object[]{tokenRequest, correlationId});
        publishedEvents.put(correlationId, new CompletableFuture<Event>());
    }

    @When("the generateTokens method is run")
    public void theGenerateTokensMethodIsRun() {
        tokenService.generateTokens(event);
    }

    @Then("the {string} event is sent with the correct amount")
    public void theTokenRepositoryOfTokenServiceContainsTokens(String arg0) throws ExecutionException, InterruptedException {
        event = publishedEvents.get(correlationId).join();
        assertEquals(arg0, event.getType());
        assertEquals(amount, event.getArgument(0, ArrayList.class).size());
    }

    @Then("the {string} event is sent instead")
    public void theEventIsSentInstead(String arg0) {
        event = publishedEvents.get(correlationId).join();
        assertEquals(arg0, event.getType());
    }

    @Given("a customerId {string} whose corresponding customer has {int} tokens")
    public void aCustomerIdWhoseCorrespondingCustomerHasTokens(String arg0, int arg1) {
        customerId = arg0;
        tokenRequest = new TokenRequest();
        tokenRequest.setCustomerId(customerId);
        tokenRequest.setAmount(arg1);
        correlationId = CorrelationId.randomId();
        event = new Event(arg0, new Object[]{tokenRequest, correlationId});
        publishedEvents.put(correlationId, new CompletableFuture<Event>());
        tokenService.generateTokens(event);
        publishedEvents = new ConcurrentHashMap<>();
        correlationId = CorrelationId.randomId();
        event = new Event(arg0, new Object[]{tokenRequest, correlationId});
        publishedEvents.put(correlationId, new CompletableFuture<Event>());
    }

    @Given("a new TokenService, two {string} events, two customerIds {string}, {string}, and two amounts {int} and {int}")
    public void aNewTokenServiceTwoEventsTwoCustomerIdsAndTwoAmountsAnd(String arg0, String arg1, String arg2, int arg3, int arg4) {
        tokenService = new TokenService(queue);
        customerId1 = arg1;
        customerId2 = arg2;
        amount1 = arg3;
        amount2 = arg4;
        tokenRequest1 = new TokenRequest();
        tokenRequest1.setCustomerId(customerId1);
        tokenRequest1.setAmount(amount1);
        tokenRequest2 = new TokenRequest();
        tokenRequest2.setCustomerId(customerId2);
        tokenRequest2.setAmount(amount2);
        correlationId1 = CorrelationId.randomId();
        correlationId2 = CorrelationId.randomId();
        event1 = new Event(arg0, new Object[]{tokenRequest1, correlationId1});
        publishedEvents.put(correlationId1, new CompletableFuture<Event>());
        event2 = new Event(arg0, new Object[]{tokenRequest2, correlationId2});
        publishedEvents.put(correlationId2, new CompletableFuture<Event>());
    }

    @When("the two events are sent at the same time")
    public void theTwoEventsAreSentAtTheSameTime() {
        var thread1 = new Thread(() -> {
            tokenService.generateTokens(event1);
        });
        var thread2 = new Thread(() -> {
            tokenService.generateTokens(event2);
        });
        thread1.start();
        thread2.start();
    }

    @Then("the the two {string} events are sent with the correct amount")
    public void theTheTwoEventsAreSentWithTheCorrectAmount(String arg0) throws Exception {
        event1 = publishedEvents.get(correlationId1).join();
        event2 = publishedEvents.get(correlationId2).join();
        assertEquals(arg0, event1.getType());
        assertEquals(arg0, event2.getType());
        assertEquals(amount2, event2.getArgument(0, ArrayList.class).size());
    }

    @And("the token has been spent")
    public void theTokenHasBeenSpent() {
        correlationId3 = CorrelationId.randomId();
        Event event = new Event("ValidateToken",new Object[]{token,correlationId3});
        publishedEvents.put(correlationId3,new CompletableFuture<Event>());
        tokenService.validateToken(event);
    }

    @When("the getCustomerId method is run")
    public void theGetCustomerIdMethodIsRun() {
        result = tokenService.getCustomerId(token);
    }

    @Then("the method should return null")
    public void theMethodShouldReturnNull() {
        assertNull(result);
    }

    public class TestMessageQueue implements MessageQueue {
        @Override
        public void publish(Event event) {
            publishedEvents.get(event.getArgument(1, CorrelationId.class)).complete(event);

        }

        @Override
        public void addHandler(String eventType, Consumer<Event> handler) {
        }
    }

}
